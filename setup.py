import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mntlogger", # Replace with your own username
    version="0.0.1",
    author="Sudipto Nandi",
    author_email="sudipto@mentorlink.ai",
    description="Logger Package",
    url="https://snandi1603@bitbucket.org/snandi1603/common-modules.git",
    packages=setuptools.find_packages(),
    python_requires='>=2.7',
    install_requires=['logger'],
)
